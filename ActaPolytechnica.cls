% Declare that this document class file requires at least LaTeX version 2e.
\NeedsTeXFormat{LaTeX2e}

% Provide the name of your document class, the date it was last updated, and a comment about what it's used for
\ProvidesClass{ActaPolytechnica}[2010/09/13]

\@twosidetrue
\@mparswitchtrue

\setlength\paperheight {250mm}%
\setlength\paperwidth  {176mm}%} 

\input{ActaPolytechnicaSize10.clo}

% Now paste your code from the preamble here.
% Replace \usepackage with \RequirePackage. (The syntax for both commands is the same.)
\RequirePackage[T1]{fontenc}
\RequirePackage{times}
\RequirePackage{setspace}
\RequirePackage{ifthen}
\RequirePackage{calc}

\newboolean{@oneauthoronly}
\setboolean{@oneauthoronly}{false}
%
\DeclareOption{oneauthoronly}{\setboolean{@oneauthoronly}{true}}

% Now we'll execute any options passed in
\ProcessOptions\relax

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\def\@authorblocktopspace{0.0ex}
\def\@affilblocktopspace{0.2mm}
\def\@affilblockinnerspace{-5mm}

\def\@authorblocktopspaceline{\cr\noalign{\vskip\@authorblocktopspace}}
\def\@affilblocktopspaceline{\cr\noalign{\vskip\@affilblocktopspace}}
\def\@affilblockinnerspaceline{\cr\noalign{\vskip\@affilblockinnerspace}}

%%%%%%%%%%%%%%%%%%
%%%%AUTHOR BLOCK %%%%%
%%%%%%%%%%%%%%%%%%
\def\authorblock#1{\relax% set the default text style
%
\global\@prevaffilblockincolfalse
\if@prevauthorblockincol\expandafter\@authorblocktopspaceline\fi
%
\global\@prevauthorblockincoltrue% we now have a block in this column
%
% input the author names
\hspace{-2mm}{\fontsize{12pt}{2pt}\selectfont \textbf{#1}}%
% end the row if the user did not already
\crcr}



%%%%%%%%%%%%%%%%%%
%%%%AFFILIATION BLOCK %%%
%%%%%%%%%%%%%%%%%%
\newcommand{\affilblock}[2]{\relax% set the default text style

\if@prevauthorblockincol\expandafter\@affilblocktopspaceline\fi
\global\@prevauthorblockincoltrue% we now have a block in this column

\if@prevaffilblockincol\expandafter\@affilblockinnerspaceline\fi
\global\@prevaffilblockincoltrue
% input the author affiliations
\hspace{-2mm}#1\hspace{1.3mm} \parbox[t]{11.5cm}{#2}%
% end the row if the user did not already
\crcr}

\newcommand{\affilblockNoNumber}[1]{\relax% set the default text style

\if@prevauthorblockincol\expandafter\@affilblocktopspaceline\fi
\global\@prevauthorblockincoltrue% we now have a block in this column

\if@prevaffilblockincol\expandafter\@affilblockinnerspaceline\fi
\global\@prevaffilblockincoltrue
% input the author affiliations
\hspace{-2mm}\parbox[t]{11.5cm}{#1}%
% end the row if the user did not already
\crcr}


\DeclareRobustCommand*{\authorrefmark}[1]{\raisebox{0pt}[0pt][0pt]{\textsuperscript{\footnotesize{#1}}}}

\newif\if@prevauthorblockincol   \@prevauthorblockincolfalse
\newif\if@prevaffilblockincol   \@prevaffilblockincolfalse


\newcommand{\keywords}[1]{\def\@keywords{#1}}
\newcommand{\mainauthor}[1]{\def\@mainauthor{#1}}
\newcommand{\runningtitle}[1]{\def\@runningtitle{#1}}
\newcommand{\superscript}[1]{ {\fontsize{9pt}{5pt}\selectfont \ensuremath{^{\textrm{#1}}}}}


\ifthenelse{\boolean{@oneauthoronly}}{
  \def\ps@myheadings{%

      \def\@evenfoot{\vadjust{\vskip -2ex \hrule} \footnotefont \hfil --\ \thepage\ -- \hfil}
      \let\@oddfoot\@evenfoot
    
      \def\@evenhead{\footnotefont \@authorOne \hfil \@runningtitle \vadjust{\vskip .15ex\hrule}}%
      \def\@oddhead{\footnotefont Acta Polytechnica Hungarica \hfil Vol. x, No. x, 20xx\vadjust{\vskip .15ex\hrule}}%

      %\let\@mkboth\@gobbletwo
      \let\sectionmark\@gobble
      \let\subsectionmark\@gobble
  }}
  {
  \def\ps@myheadings{%

      \def\@evenfoot{\vadjust{\vskip -2ex \hrule} \footnotefont \hfil --\ \thepage\ -- \hfil}
      \let\@oddfoot\@evenfoot
    
      \def\@evenhead{\footnotefont \@mainauthor \ {\itshape et al.} \hfil \@runningtitle \vadjust{\vskip .15ex\hrule}}%
      \def\@oddhead{\footnotefont Acta Polytechnica Hungarica \hfil Vol. x, No. x, 20xx\vadjust{\vskip .15ex\hrule}}%

      %\let\@mkboth\@gobbletwo
      \let\sectionmark\@gobble
      \let\subsectionmark\@gobble
  }}
%\fi

\newcommand\maketitle{\par
  \begingroup
    \renewcommand\thefootnote{\@fnsymbol\c@footnote}%
    \def\@makefnmark{\rlap{\@textsuperscript{\normalfont\@thefnmark}}}%
    \long\def\@makefntext##1{\parindent 1em\noindent
            \hb@xt@1.8em{%
                \hss\@textsuperscript{\normalfont\@thefnmark}}##1}%
    \if@twocolumn
      \ifnum \col@number=\@ne
        \@maketitle
      \else
        \twocolumn[\@maketitle]%
      \fi
    \else
      \newpage
      \global\@topnum\z@   % Prevents figures from going at top of page.
      \@maketitle
    \fi
    \thispagestyle{myheadings}\@thanks
  \endgroup
  \setcounter{footnote}{0}%
  \global\let\thanks\relax
  \global\let\maketitle\relax
  \global\let\@maketitle\relax
  \global\let\@thanks\@empty
  \global\let\@author\@empty
  \global\let\@date\@empty
  \global\let\@title\@empty
  \global\let\title\relax
  \global\let\author\relax
  \global\let\date\relax
  \global\let\and\relax
}

\def\@maketitle{%
	\newpage
	\null
	\begin{flushleft}%
		\begin{spacing}{1.6}
		\vskip -3mm
		{\fontsize{16}{250}\selectfont \textbf{\@title}}%
		\end{spacing}
		\vskip 5mm%
		
		 \@prevauthorblockincolfalse
		 \@prevaffilblockincolfalse
		\begin{tabular}{l}
			\@author
		\end{tabular}
		\addtolength{\parindent}{2mm}		
	\end{flushleft}%
}


\setcounter{secnumdepth}{3}
\newcounter {part}
\newcounter {section}
\newcounter {subsection}[section]
\newcounter {subsubsection}[subsection]
\newcounter {paragraph}[subsubsection]
\newcounter {subparagraph}[paragraph]
\renewcommand \thepart {\@Roman\c@part}
\renewcommand \thesection {\@arabic\c@section}
\renewcommand\thesubsection   {\thesection.\@arabic\c@subsection}
\renewcommand\thesubsubsection{\thesubsection.\@arabic\c@subsubsection}
\renewcommand\theparagraph    {\thesubsubsection.\@arabic\c@paragraph}
\renewcommand\thesubparagraph {\theparagraph.\@arabic\c@subparagraph}
\newcommand\part{%
   \if@noskipsec \leavevmode \fi
   \par
   \addvspace{4ex}%
   \@afterindentfalse
   \secdef\@part\@spart}

\def\@part[#1]#2{%
    \ifnum \c@secnumdepth >\m@ne
      \refstepcounter{part}%
      \addcontentsline{toc}{part}{\thepart\hspace{1em}#1}%
    \else
      \addcontentsline{toc}{part}{#1}%
    \fi
    {\parindent \z@ \raggedright
     \interlinepenalty \@M
     \normalfont
     \ifnum \c@secnumdepth >\m@ne
       \Large\bfseries \partname\nobreakspace\thepart
       \par\nobreak
     \fi
     \huge \bfseries #2%
     \markboth{}{}\par}%
    \nobreak
    \vskip 3ex
    \@afterheading}
\def\@spart#1{%
    {\parindent \z@ \raggedright
     \interlinepenalty \@M
     \normalfont
     \huge \bfseries #1\par}%
     \nobreak
     \vskip 3ex
     \@afterheading}
\newcommand\section{\@startsection {section}{1}{\z@}%
				%used to be plus -1ex
                                   {-3.5ex \@plus -0.2ex \@minus -.2ex}%
                                   {2.3ex \@plus.2ex}%
                                   {\normalfont\Large\bfseries}}
\newcommand\subsection{\@startsection{subsection}{2}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\large\bfseries}}
\newcommand\subsubsection{\@startsection{subsubsection}{3}{\z@}%
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\normalsize\bfseries}}
\newcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
                                    {3.25ex \@plus1ex \@minus.2ex}%
                                    {-1em}%
                                    {\normalfont\normalsize\bfseries}}
\newcommand\subparagraph{\@startsection{subparagraph}{5}{\parindent}%
                                       {3.25ex \@plus1ex \@minus .2ex}%
                                       {-1em}%
                                      {\normalfont\normalsize\bfseries}}


\setlength\leftmargini  {2.5em}
\leftmargin  \leftmargini
\setlength\leftmarginii  {2.2em}
\setlength\leftmarginiii {1.87em}
\setlength\leftmarginiv  {1.7em}
\setlength\leftmarginv  {1em}
\setlength\leftmarginvi {1em}

\setlength\parindent {0pt}
\setlength\parskip {6pt}

\setlength  \labelsep  {.5em}
\setlength  \labelwidth{\leftmargini}
\addtolength\labelwidth{-\labelsep}
\@beginparpenalty -\@lowpenalty
\@endparpenalty   -\@lowpenalty
\@itempenalty     -\@lowpenalty
\renewcommand\theenumi{\@arabic\c@enumi}
\renewcommand\theenumii{\@alph\c@enumii}
\renewcommand\theenumiii{\@roman\c@enumiii}
\renewcommand\theenumiv{\@Alph\c@enumiv}
\newcommand\labelenumi{\theenumi.}
\newcommand\labelenumii{(\theenumii)}
\newcommand\labelenumiii{\theenumiii.}
\newcommand\labelenumiv{\theenumiv.}
\renewcommand\p@enumii{\theenumi}
\renewcommand\p@enumiii{\theenumi(\theenumii)}
\renewcommand\p@enumiv{\p@enumiii\theenumiii}
\newcommand\labelitemi{\textbullet}
\newcommand\labelitemii{\normalfont\bfseries \textendash}
\newcommand\labelitemiii{\textasteriskcentered}
\newcommand\labelitemiv{\textperiodcentered}
\newenvironment{description}
               {\list{}{\labelwidth\z@ \itemindent-\leftmargin
                        \let\makelabel\descriptionlabel}}
               {\endlist}
\newcommand*\descriptionlabel[1]{\hspace\labelsep
                                \normalfont\bfseries #1}



\renewcommand\descriptionlabel[1]%
	{\hspace{\labelsep}\textit{#1}}



\RequirePackage{calc}
\newenvironment{altDescription}[1][\quad]
			{\begin{list}{}{
   				\renewcommand\makelabel[1]{\hfil\textsf{##1}}
   				\settowidth\labelwidth{\makelabel{#1}}
   				\setlength\leftmargin{\labelwidth+\labelsep-5pt}
   				\renewcommand{\makelabel}{\descriptionlabel}}}
  			{\end{list}}

\newenvironment{abstract}{
			{\vskip 5mm \relax \rule{\textwidth}{0.1pt}}
			\vspace{-27pt}
			\setstretch{0.95}
			\abstractfont
			\begin{altDescription}[]
				\item[Abstract:]%\itshape\selectfont
      		}
      		{\end{altDescription}{%\vskip .15ex \small
			\begin{altDescription}[]
			\item[Keywords:] \normalfont\itshape \@keywords \end{altDescription} \vskip -8mm \relax \rule{\textwidth}{0.1pt}} \vskip 9mm}% \setstretch{1}}


%\fi
\newenvironment{verse}
               {\let\\\@centercr
                \list{}{\itemsep      \z@
                        \itemindent   -1.5em%
                        \listparindent\itemindent
                        \rightmargin  \leftmargin
                        \advance\leftmargin 1.5em}%
                \item\relax}
               {\endlist}
\newenvironment{quotation}
               {\list{}{\listparindent 1.5em%
                        \itemindent    \listparindent
                        \rightmargin   \leftmargin
                        \parsep        \z@ \@plus\p@}%
                \item\relax}
               {\endlist}
\newenvironment{quote}
               {\list{}{\rightmargin\leftmargin}%
                \item\relax}
               {\endlist}
               
\newenvironment{nonSection}[1]
	{\textbf{\fontsize{10pt}{15pt}#1}}{}
	


\setlength\arraycolsep{5\p@}
\setlength\tabcolsep{6\p@}
\setlength\arrayrulewidth{.4\p@}
\setlength\doublerulesep{2\p@}
\setlength\tabbingsep{\labelsep}
\skip\@mpfootins = \skip\footins
\setlength\fboxsep{3\p@}
\setlength\fboxrule{.4\p@}
\renewcommand \theequation {\@arabic\c@equation}
\newcounter{figure}
\renewcommand \thefigure {\@arabic\c@figure}
\def\fps@figure{tbp}
\def\ftype@figure{1}
\def\ext@figure{lof}
\def\fnum@figure{\figurename\nobreakspace\thefigure}
\newenvironment{figure}
               {\@float{figure}}
               {\end@float}
\newenvironment{figure*}
               {\@dblfloat{figure}}
               {\end@dblfloat}

\newcounter{table}
\renewcommand\thetable{\@arabic\c@table}
\def\fps@table{tbp}
\def\ftype@table{2}
\def\ext@table{lot}
\def\fnum@table{\tablename\nobreakspace\thetable}
\newenvironment{table}
               {\@float{table}}
               {\end@float}
\newenvironment{table*}
               {\@dblfloat{table}}
               {\end@dblfloat}
\newlength\abovecaptionskip
\newlength\belowcaptionskip
\setlength\abovecaptionskip{10\p@}
\setlength\belowcaptionskip{0\p@}


\long\def\@makecaption#1#2{%
  \vskip\abovecaptionskip
  \begin{center}
  \sbox\@tempboxa{\footnotesize #1 #2}%
%  \ifdim \wd\@tempboxa >\hsize
    \footnotesize#1\break \footnotesize#2\par
%  \else
%    \global \@minipagefalse
%    \hb@xt@\hsize{\hfil\box\@tempboxa\hfil}%
%  \fi
  \end{center}
  \vskip\belowcaptionskip}



\DeclareOldFontCommand{\rm}{\normalfont\rmfamily}{\mathrm}
\DeclareOldFontCommand{\sf}{\normalfont\sffamily}{\mathsf}
\DeclareOldFontCommand{\tt}{\normalfont\ttfamily}{\mathtt}
\DeclareOldFontCommand{\bf}{\normalfont\bfseries}{\mathbf}
\DeclareOldFontCommand{\it}{\normalfont\itshape}{\mathit}
\DeclareOldFontCommand{\sl}{\normalfont\slshape}{\@nomath\sl}
\DeclareOldFontCommand{\sc}{\normalfont\scshape}{\@nomath\sc}
\DeclareRobustCommand*\cal{\@fontswitch\relax\mathcal}
\DeclareRobustCommand*\mit{\@fontswitch\relax\mathnormal}

%\newcommand\@pnumwidth{1.55em}
%\newcommand\@tocrmarg{2.55em}
%\newcommand\@dotsep{4.5}
%\setcounter{tocdepth}{3}
%\newcommand\tableofcontents{%
%    \section*{\contentsname
%        \@mkboth{%
%           \MakeUppercase\contentsname}{\MakeUppercase\contentsname}}%
%    \@starttoc{toc}%
%    }
%\newcommand*\l@part[2]{%
%  \ifnum \c@tocdepth >-2\relax
%    \addpenalty\@secpenalty
%    \addvspace{2.25em \@plus\p@}%
%    \setlength\@tempdima{3em}%
%    \begingroup
%      \parindent \z@ \rightskip \@pnumwidth
%      \parfillskip -\@pnumwidth
%      {\leavevmode
%       \large \bfseries #1\hfil \hb@xt@\@pnumwidth{\hss #2}}\par
%       \nobreak
%       \if@compatibility
%         \global\@nobreaktrue
%         \everypar{\global\@nobreakfalse\everypar{}}%
%      \fi
%    \endgroup
%  \fi}
\newcommand*\l@section[2]{%
  \ifnum \c@tocdepth >\z@
    \addpenalty\@secpenalty
    \addvspace{1.0em \@plus\p@}%
    \setlength\@tempdima{1.5em}%
    \begingroup
      \parindent \z@ \rightskip \@pnumwidth
      \parfillskip -\@pnumwidth
      \leavevmode \bfseries
      \advance\leftskip\@tempdima
      \hskip -\leftskip
      #1\nobreak\hfil \nobreak\hb@xt@\@pnumwidth{\hss #2}\par
    \endgroup
  \fi}
\newcommand*\l@subsection{\@dottedtocline{2}{1.5em}{2.3em}}
\newcommand*\l@subsubsection{\@dottedtocline{3}{3.8em}{3.2em}}
\newcommand*\l@paragraph{\@dottedtocline{4}{7.0em}{4.1em}}
\newcommand*\l@subparagraph{\@dottedtocline{5}{10em}{5em}}
\newcommand\listoffigures{%
    \section*{\listfigurename}%
      \@mkboth{\MakeUppercase\listfigurename}%
              {\MakeUppercase\listfigurename}%
    \@starttoc{lof}%
    }
\newcommand*\l@figure{\@dottedtocline{1}{1.5em}{2.3em}}
\newcommand\listoftables{%
    \section*{\listtablename}%
      \@mkboth{%
          \MakeUppercase\listtablename}%
         {\MakeUppercase\listtablename}%
    \@starttoc{lot}%
    }
\let\l@table\l@figure


\newdimen\bibindent
\setlength\bibindent{1.5em}
\newenvironment{thebibliography}[1]
%     {\section*{\refname}%
	{\textbf{\fontsize{10pt}{15pt}\refname}
      \@mkboth{\MakeUppercase\refname}{\MakeUppercase\refname}%
      \list{\@biblabel{\@arabic\c@enumiv}}%
           {\settowidth\labelwidth{\@biblabel{#1}}%
            \leftmargin\labelwidth
            \advance\leftmargin\labelsep
            \@openbib@code
            \usecounter{enumiv}%
            \let\p@enumiv\@empty
            \renewcommand\theenumiv{\@arabic\c@enumiv}}%
      \sloppy
      \clubpenalty4000
      \@clubpenalty \clubpenalty
      \widowpenalty4000%
      \sfcode`\.\@m}
     {\def\@noitemerr
       {\@latex@warning{Empty `thebibliography' environment}}%
      \endlist}
\newcommand\newblock{\hskip .11em\@plus.33em\@minus.07em}
\let\@openbib@code\@empty


%\newenvironment{theindex}
%               {\if@twocolumn
%                  \@restonecolfalse
%                \else
%                  \@restonecoltrue
%                \fi
%                \twocolumn[\section*{\indexname}]%
%                \@mkboth{\MakeUppercase\indexname}%
%                        {\MakeUppercase\indexname}%
%                \thispagestyle{myheadings}\parindent\z@
%                \parskip\z@ \@plus .3\p@\relax
%                \columnseprule \z@
%                \columnsep 35\p@
%                \let\item\@idxitem}
%               {\if@restonecol\onecolumn\else\clearpage\fi}
%\newcommand\@idxitem{\par\hangindent 40\p@}
%\newcommand\subitem{\@idxitem \hspace*{20\p@}}
%\newcommand\subsubitem{\@idxitem \hspace*{30\p@}}
%\newcommand\indexspace{\par \vskip 10\p@ \@plus5\p@ \@minus3\p@\relax}
\renewcommand\footnoterule{%
 \vskip 5mm
  \kern-3\p@
  \hrule\@width.4\columnwidth
  \kern2.6\p@}
\newcommand\@makefntext[1]{%
    \indent
    \@makefnmark \hspace{5mm} \parbox[t]{11.5cm}{#1}}


%\newcommand\contentsname{Contents}
%\newcommand\listfigurename{List of Figures}
%\newcommand\listtablename{List of Tables}
\newcommand\refname{References}
%\newcommand\indexname{Index}
\newcommand\figurename{Figure}
\newcommand\tablename{Table}
\newcommand\partname{Part}
%\newcommand\appendixname{Appendix}
\newcommand\abstractname{Abstract}

\setlength\columnsep{10\p@}
\setlength\columnseprule{0\p@}
\pagestyle{myheadings}
\pagenumbering{arabic}

\onecolumn

\endinput
%%
%% End of file `article.cls'.






% Finally, we'll use \endinput to indicate that LaTeX can stop reading this file. LaTeX will ignore anything after this line.
\endinput
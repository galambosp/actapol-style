
filename = samplePaper

toTrash = $(filename).ps $(auxes) $(bbls) $(blgs) $(filename).aux $(filename).log $(filename).synctex.gz $(filename).dvi
toTrashAll = $(toTrash) $(filename).pdf

.SUFFIXES:
.SUFFIXES: aux blg bbl

clean:
	rm -f $(toTrash)

cleanall:
	rm -rf $(toTrashAll)

all:
	make cleanall
	xelatex -papersize=b5 $(filename)
	bibtex $(filename)
	xelatex -papersize=b5 $(filename)
	xelatex -papersize=b5 $(filename)
	make clean


	
